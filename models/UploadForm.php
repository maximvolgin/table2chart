<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $htmlFile;

    public function rules()
    {
        return [
            [['htmlFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html'],
//              [['htmlFile'], 'file'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'htmlFile' => 'Report File to build chart',
        ];
    }
    
    public function upload()
    {
        
        if ($this->validate()) {
            $this->htmlFile->saveAs('../uploads/' . $this->htmlFile->baseName . '.' . $this->htmlFile->extension);
            
            return true;
            
        } else {
            
            return false;
        }
    }
}

