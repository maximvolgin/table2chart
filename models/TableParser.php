<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

class TableParser {
    
    static function getData( $filename ) {
        $result = [];

        $data = file_get_contents('../uploads/'.$filename );

        
        $dom = new \domDocument;

        @$dom->loadHTML($data);

//        var_dump( $dom );
//        exit;
            
        $dom->preserveWhiteSpace = false;
        $tables = $dom->getElementsByTagName('table');

        $table = $tables->item(0);
        
        if ( $table === null ) throw new \Exception("Incorrect file format");
        
        $rows = $tables->item(0)->getElementsByTagName('tr');

        $start_balance = 0; // В таблице его нет хотя должен быть примем нулем
        
        foreach ($rows as $row) {
            
            $cols = $row->getElementsByTagName('td');
            
            $line = [];
            
            foreach ( $cols as $col ) {
                $line[] = $col->textContent;
            }
            
//            print_r( $line );
            
            if ( isset ( $line[2] ) ) {
                
                $date = str_replace('.', '-', $line[1] );
                
                switch ( $line[2] ) {
                    case 'buy':  //покупка
                    case 'sell': //продажи в примере нет но должно быть точно также
                      if ( !isset( $line[13] ) ) throw new \Exception( "Broken table");
                      $start_balance = $start_balance + (float)preg_replace('/\s+/', '', $line[13] );
                      $result[] = [ 'date' => $date, 'balance' => $start_balance , 'description' => $line[2].' '.$line[4].': '.$line[13] ];
                    break;
                    case 'balance': // балансовая операция (пополнение или вывод)
                      if ( !isset( $line[4] ) ) throw new \Exception( "Broken table");
                      $start_balance = $start_balance + (float)preg_replace('/\s+/', '', $line[4] );
                      $result[] = [ 'date' => $date, 'balance' => $start_balance , 'description' => $line[3].': '.$line[4] ];
                    break;
                
                    // все остальное пофиг
                }
                
            }
            
        }
        
        if ( count( $result ) == 0  ) throw new \Exception( "No data in file");
        
        return $result;
    }
}