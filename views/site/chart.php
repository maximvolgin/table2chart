<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<pre>
<?php
print_r( $chart );
?>
</pre>
<div id="chart" style="height: 400px"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart'], 'language': 'ru'});
    
    var width = '80%';
    var balance_options = {
        hAxis: {
            baselineColor: '#F1F1F1',
            gridlines: {color: 'transparent'},
            textStyle: {color: '#66818D',fontName: 'Source Sans Pro',fontSize: '15px'}
        },
        vAxis: {
            minValue: 0,
            format: '#,### USD',
            baselineColor: '#F1F1F1',
            gridlines: {color: '#F1F1F1'},
        textStyle: {
            color: '#66818D',
            fontName: 'Source Sans Pro',
            fontSize: '15px'
        }
    },
    colors: ['#E65113'],
    legend: {position: 'none'},
    chartArea: {width: width, height: '80%'},
    areaOpacity: 0.1,
        tooltip: {
            textStyle: {color: '#333'},
            showColorCode: true
        }
    };


    google.charts.setOnLoadCallback(drawChart);
    
    function drawChart() {

        var aData = [
              ['Дата', 'Баланс'],
              <?php foreach ( $chart as $point ) : ?>
                  <?= "[ new Date('".$point[ 'date' ]."'), ".$point[ 'balance' ]."],\n" ?>      
              <?php endforeach; ?>
        ];

        if ( aData.length > 1 ) {

            var data = google.visualization.arrayToDataTable( aData );
            
            var balance_formatter = new google.visualization.NumberFormat({
                fractionDigits: 2,
                suffix: ' USD'
            });

            balance_formatter.format( data, 1);

            var chart = new google.visualization.AreaChart(document.getElementById('chart'));
            chart.draw( data, balance_options);

        }
    }
    
</script>
